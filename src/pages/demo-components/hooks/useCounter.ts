import {useState} from "react";

export function useCounter(initialValue = 0) {
  const [counter, setCounter] = useState(initialValue)

  function inc() {
    setCounter((prev) => {
      return prev + 1
    })
  }

  return {
    counter,
    increment: inc
  }

}
