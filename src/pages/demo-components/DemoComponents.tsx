import {useEffect, useState} from "react";
import {Card, CardBody, CardTitle} from "../../uikit/Card.tsx";
import {Panel} from "../../uikit/Panel.tsx";
import {useCounter} from "./hooks/useCounter.ts";

const initialState = [
  { id: 1, name: 'Fabio'},
  { id: 2, name: 'Ciccio'},
  { id: 3, name: 'Pluto'},
]

export function DemoComponents() {
  const { counter, increment } = useCounter()

  const [users, setUsers] = useState(initialState)
  const [visible, setVisible] = useState(false)

  function openUrl(url: string) {
    window.open(url)
  }
  function doSomething() {
    console.log('do something')
  }

  const getTitle = () => 'super titolo'

  console.log('render')
  return (
    <div className="m-2">
      <div className="title">

        {getTitle()}

        <button onClick={increment}>
          count is {counter}
        </button>

        <Panel
          title="my profile"
          icon="fa fa-google"
          onIconClick={() => openUrl('http://www.google.com')}
        >
          bla bla
        </Panel>

        <Panel
          title="my profile"
          icon="fa fa-eye"
          onIconClick={doSomething}
        >
          lorem ipsum
        </Panel>

        <br />

        {visible && <Card title="ciao">
            <Card.Title>ONE</Card.Title>
            <Card.Body>lroem ipsum</Card.Body>
        </Card>}

        <button
          disabled={true}
          className="bg-pink-500 rounded-2xl px-3 py-1 text-white hover:bg-pink-700 transition disabled:opacity-30"
        > CLICK </button>
      </div>
    </div>
  )
}



/*

document.getElementById('btn')
  .addEventListener('click', function(e: MouseEvent) {
    console.log('ciao')
  })


function inc(e: MouseEvent) {
  console.log('ciao')
}

document.getElementById('btn')
  .addEventListener('click', inc)*/
