import {useEffect, useState} from "react";
import axios from "axios";
import {User} from "../../../model/user.ts";

const API = 'http://localhost:3000'

interface UsersState {
  list: User[];
  error: string | null;
  pending: boolean
}


const initialState: UsersState = {
  list: [],
  error: null,
  pending: false
}
export function useUsers() {
  const [data, setData] = useState(initialState)
  const [selectedItem, setSelectedItem] = useState<User | null>(null)
  const [modalIsOpen, setModalIsOpen] = useState(false)

  useEffect(() => {
    setData(s => ({...s, pending: false}))
    axios.get<User[]>(`${API}/users`)
      .then(res => {
        setData((s) => ({
          ...s,
          pending: false,
          list: res.data
        }));
      })
      .catch(() => setData((s) => ({
        ...s,
        error: 'list not loading',
        pending: false
      })))
  }, [])

  function deleteUser(userToDelete: number) {
    setData(s => ({...s, error: null, pending: true}))

    axios.delete(`${API}/users/${userToDelete}`)
      .then(() => {
        setData(s => ({
          ...s,
          pending: false,
          list: s.list?.filter(u => u.id !== userToDelete)
        }))
      })
      .catch(() => setData(s => ({...s, error: 'delete failed', pending: false})))

  }

  function addUserHandler(formData: Partial<User>) {
    setData(s => ({...s, error: null, pending: true}))
    axios.post<User>(`${API}/users/`, formData)
      .then(res => {
        setData(s => ({
          ...s,
          list: [...s.list, res.data],
          pending: false
        }))
      })
  }

  function editUserHandler(formData: Partial<User>) {
    setData(s => ({...s, error: null, pending: true}))
    axios.patch<User>(`${API}/users/${selectedItem?.id}`, formData)
      .then(res => {
        setData(s => ({
          ...s,
          list: s.list.map(u => {
            if (u.id === formData?.id) {
              return res.data;
            }
            return u
          }),
          pending: false
        }))
        closeModal()
      })
  }

  function setActiveUser(user: User) {
    setModalIsOpen(true)
    setSelectedItem(user)
  }

  function clearActiveUser() {
    setSelectedItem(null)
  }

  function openModal() {
    setModalIsOpen(true)
  }

  function closeModal() {
    clearActiveUser()
    setModalIsOpen(false)
  }

  function save(formData: Partial<User>) {
    if (selectedItem?.id) {
      editUserHandler(formData)
    } else {
      addUserHandler(formData)
    }
  }

  return {
    data,
    modalIsOpen,
    selectedItem,
    actions: {
      save,
      deleteUser,
      setActiveUser,
      clearActiveUser,
      openModal,
      closeModal
    }

  }
}
