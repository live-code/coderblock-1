import {User} from "../../../model/user.ts";
import {UsersListItem} from "./UsersListItem.tsx";

interface UsersListProps {
  items: User[];
  onSelectItem: (user: User) => void;
  onDeleteItem: (userId: number) => void;
}

export function UsersList(props: UsersListProps) {
  return <>
    {
      props.items?.map((u) => {
        return (
          <UsersListItem
            key={u.id}
            user={u}
            onSelectItem={props.onSelectItem}
            onDeleteItem={props.onDeleteItem}
          />
        )
      })
    }
  </>
}
