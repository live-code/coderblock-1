import {User} from "../../../model/user.ts";
import {useState} from "react";

interface UsersListItemProps {
  user: User;
  onSelectItem: (user: User) => void;
  onDeleteItem: (userId: number) => void;
}
export function UsersListItem(props:UsersListItemProps) {
  const [open, setOpen] = useState(false)
  const { user: u } = props;
  return (
    <div >
      <div
        className="flex justify-between"

        onClick={() => props.onSelectItem(u)}
      >
        {u.name} {u.username}
        <div className="flex gap-2 w-8">
          <i
            className="fa fa-trash"
            onClick={(e) => {
              e.stopPropagation()
              props.onDeleteItem(u.id)
            }}
          />
          <i className="fa fa-arrow-circle-down" onClick={(e) => {
            e.stopPropagation()
            setOpen(s => !s)
          }}></i>
        </div>
      </div>
      {
        open && <div>
              body...
          </div>
      }
    </div>
  )
}








/*
const users = [

  { name: 'A', children: ['B', 'C']},
  { name: 'B', children: []},
  { name: 'C', children: ['C', 'D']}
]*/
