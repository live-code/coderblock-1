import clsx from "clsx";
import React, {FormEvent, useEffect, useState} from "react";
import {ToJson} from "../../../uikit/ToJson.tsx";
import {User} from "../../../model/user.ts";

interface UsersFormProps {
  selectedItem: User | null;
  onSubmit: (formData: Partial<User>) => void,
}

const initialState = { name: '', username: ''}

export function UsersForm(props: UsersFormProps) {
  const [formData, setFormData] = useState<Partial<User>>(initialState)

  useEffect(() => {
    if (props.selectedItem?.id) {
      setFormData(props.selectedItem)
    } else {
      setFormData(initialState)
    }

  }, [props.selectedItem?.id ])

  function changeHandler(e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) {
    const value = e.currentTarget.value
    const name = e.currentTarget.name
    const type = e.currentTarget.type
    setFormData(s => ({...s, [name]: type === 'number' ? +value : value}))
  }
  function submitHandler(e: FormEvent<HTMLFormElement>) {
    e.preventDefault()
    props.onSubmit(formData)
  }

  const isNameValid = formData.name && formData.name.length > 0
  const isUsernameValid = formData.username && formData.username.length > 0
  const isValid = isNameValid && isUsernameValid

  return  <form
    onSubmit={submitHandler}
  >

    {!isNameValid &&  <div>name required</div>}

    <input
      type="text"
      placeholder="name"
      value={formData.name}
      name="name"
      className={clsx(
        'myInput',
        {
          'error': !isNameValid,
          'valid': isNameValid,
        })}
      onChange={changeHandler}
    />

    <input
      type="text"
      placeholder="name"
      value={formData.username}
      name="username"
      onChange={changeHandler}
    />

    <button type="button">CLEAR</button>
    <button type="submit" disabled={!isValid} className="btn">
      {formData?.id ? 'EDIT' : 'ADD'}
    </button>

    <ToJson data={formData} />
    <hr/>
    <ToJson data={props.selectedItem} />

  </form>

}
