import {UsersList} from "./components/UsersList.tsx";
import {UsersForm} from "./components/UsersForm.tsx";
import {useUsers} from "./hooks/useUsers.ts";
import {Modal} from "../../uikit/Modal.tsx";

export function DemoCrud() {
  const {data, modalIsOpen, selectedItem, actions} = useUsers();

  return <div>
    <h1>
      Lista utenti
      <i className="fa fa-plus-circle"
         onClick={actions.openModal}></i>
    </h1>

    {data.error && <div>{data.error}</div>}
    {data.pending && <div>loader....</div>}

    <Modal
      show={modalIsOpen}
      onClose={actions.closeModal}
    >
      <UsersForm
        selectedItem={selectedItem}
        onSubmit={actions.save}/>
    </Modal>

   <UsersList
     items={data.list}
     onSelectItem={actions.setActiveUser}
     onDeleteItem={actions.deleteUser}
   />




  </div>
}

