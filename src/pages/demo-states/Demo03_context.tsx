import React, {PropsWithChildren, useCallback, useState} from "react";

export function Demo02_state() {
  const [value1, setValue1] = useState(0)
  const [value2, setValue2] = useState(1)

  const inc = useCallback(() => {
    setValue1(s => s + 1)
  }, [])

  console.log('Root', value1, value2)
  return <div className="comp">
    <h1>Demo01_state</h1>
    <button onClick={inc}>+</button>
    <button onClick={() => setValue2(Math.random())}>random</button>
    <Layout title="blog">
      <Panel1 value={value1}/>
      <Panel2 value={value2} />
      <Panel3 increment={inc} />
    </Layout>
  </div>
}

export function Layout(props: PropsWithChildren<{ title: string}>) {
  console.log(' Layout')
  return <div className="comp">
    <h1>{props.title}</h1>
    {props.children}
  </div>
}

function  Panel1Func(props: { value: number }) {
  console.log('  Panel1')

  return <div className="comp">
    <h1>Panel1 {props.value}</h1>
  </div>
}

export const Panel1 = React.memo(Panel1Func)


interface Panel2Props {
  value: number;
}
const Panel2 = React.memo((props: Panel2Props) => {
  console.log('  Panel2')
  return <div className="comp">
    <h1>Panel2 {props.value}</h1>
  </div>
})

interface Panel3Props {
  increment: () => void;
}
const Panel3 = React.memo((props: Panel3Props) => {
  console.log('  Panel3')
  return <div className="comp">
    <h1>Panel3</h1>

    <button onClick={props.increment}>Update Value 1</button>
  </div>
})
