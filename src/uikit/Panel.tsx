import {PropsWithChildren} from "react";

type PanelProps = {
  title: string;
  icon?: string;
  url?: string;
  onIconClick?: () => void;
}

export function Panel(props: PropsWithChildren<PanelProps>) {
  const { onIconClick, icon, title = 'widget' } = props;

  return <div>
    <div className="flex justify-between items-center bg-sky-600 text-white px-3 py-1">
      {title}
      {
        icon &&
          <i
              className={icon}
              onClick={() => onIconClick}></i>
      }
    </div>
    <div className="border border-sky-600 p-3">{props.children}</div>
  </div>
}
