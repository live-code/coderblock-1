import clsx from "clsx";
import {PropsWithChildren} from "react";

interface ModalProps {
  show: boolean;
  onClose: () => void;
}

export function Modal(props: PropsWithChildren<ModalProps>) {
  return (
    <div className={clsx(
      'fixed bg-gray-200 top-0 left-0 w-full h-full transition',
      {
        'opacity-0 pointer-events-none': !props.show,
      })
    }>
      <i className="fa fa-times" onClick={props.onClose}></i>
      {props.children}
    </div>
  )
}
