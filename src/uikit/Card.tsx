import {PropsWithChildren} from "react";

type CardProps = {
  title?: string;
}

export function Card(props: PropsWithChildren<CardProps>) {

  return <div>
    {props.children}
  </div>
}


export function CardTitle(props: PropsWithChildren) {
  return <div className="bg-sky-600 text-white px-3 py-1">
    {props.children}
  </div>
}


export function CardBody(props: PropsWithChildren) {
  return <div className="border border-sky-600 p-3">
    {props.children}
  </div>
}

Card.Title = CardTitle;
Card.Body = CardBody;
